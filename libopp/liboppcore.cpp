
#include "liboppcore.h"

#include <intrin.h>
#include <array>
#include <bitset>
#include <map>

using namespace libopp;



Size::Size() : 
    width(0), 
    height(0)
{

}


Size::Size(int64_t w, int64_t h) :
    width(w),
    height(h)
{

}


Size::Size(const std::initializer_list<int64_t>& init)
{
    if (init.size() != 2)
    {
        throw std::invalid_argument("size does not contain two elements");
    }

    auto i = init.begin();
    width = *i;
    i++;
    height = *i;
}


bool Size::operator== (const Size& s) const
{
    return (width == s.width && height == s.height);
}


bool Size::operator != (const Size& s) const
{
    return (width != s.width || height != s.height);
}


const Point Point::Origin = Point();


Point::Point() : 
    x(0), 
    y(0)
{

}


Point::Point(int64_t x, int64_t y) :
    x(x),
    y(y)
{

}


Point::Point(const std::initializer_list<int64_t>& init)
{
    if (init.size() != 2)
    {
        throw std::invalid_argument("Initialization list is wrong size");
    }

    auto i = init.begin();
    x = *i;
    i++;

    y = *i;
}


bool Point::operator == (const Point& pt) const
{
    return x == pt.x && y == pt.y;
}


bool Point::operator != (const Point& pt) const
{
    return x != pt.x || y != pt.y;
}



Rect::Rect() :
    Size(),
    Point()
{

}


Rect::Rect(int64_t x, int64_t y, int64_t w, int64_t h) :
    Point(x, y),
    Size(w, h)
{

}


Rect::Rect(const Point& pt, const Size& size) :
    Point(pt),
    Size(size)
{

}


bool Rect::operator== (const Rect& a) const
{
    return Size::operator== (*this) && 
           Point::operator == (*this);
}


bool Rect::operator != (const Rect& a) const
{
    return Size::operator != (*this) ||
           Point::operator != (*this);
}


Point Rect::upperLeft() const
{
    return Point(x, y);
}


Point Rect::upperRight() const
{
    return Point(x + width - 1, y);
}


Point Rect::lowerRight() const
{
    return Point(x + width - 1, y + height - 1);
}


Point Rect::lowerLeft() const
{
    return Point(x, y + height - 1);
}


bool Rect::inside(const Rect& r) const
{
    if (r.x + r.width > width)
        return false;

    if (r.y + r.height > height)
        return false;

    return true;
}


void libopp::Validate(int expr)
{
    if (!expr)
        throw OppException("Validate failed");
}




void OppImgDeleter::operator()(void* buf)
{
    _aligned_free(buf);
}




OppException::OppException() : 
    _exceptionName("OppException")
{

}


OppException::OppException(const std::string& msg) : 
    OppException()
{
    _msg = _exceptionName + ": " + msg;
}


OppException::OppException(const std::string& exceptionName, const std::string& msg) :
    _exceptionName(exceptionName)
{
    _msg = _exceptionName + ": " + msg;
}


const char* OppException::what() const
{
    return _msg.c_str();
}



NotImplementedException::NotImplementedException() :
    OppException("NotImplementedException")
{

}


NotImplementedException::NotImplementedException(const std::string& msg) :
    OppException(msg)
{

}



