

#ifndef LIBOPP_CPUINFO_H
#define LIBOPP_CPUINFO_H

#include <string>
#include <vector>


namespace libopp
{
    class CpuInfo
    {
    public:
        CpuInfo();

        bool mmx() const;
        bool sse() const;
        bool sse2() const;
        bool sse3() const;
        bool ssse3() const;
        bool fma() const;
        bool sse41() const;
        bool sse42() const;
        bool aes() const;
        bool avx() const;
        bool avx2() const;
        bool bmi1() const;
        bool bmi2() const;

        bool avx512f() const;
        bool avx512dq() const;
        bool avx512ifma() const;
        bool avx512pf() const;
        bool avx512er() const;
        bool avx512cd() const;
        bool avx512bw() const;
        bool avx512vl() const;
        bool avx512bmi() const;
        bool avx512bmi2() const;
        bool avx512nn() const;
        bool avx512bitalg() const;
        bool avx512_4nniw() const;
        bool avx512_4fmaps() const;
        bool avx512vp2intersect() const;

        bool fp16() const;


        const std::string manufacturer() const;

        int family() const;
        int model() const;
        int stepping() const;


        static std::vector<unsigned int> GetCpuInfo(unsigned int infoType, unsigned int subFeature = 0);

        static const CpuInfo& cpuInfo();

    protected:
        static const CpuInfo _cpuInfo;
        static int GetBits(int d, int startPos, int endPos);

    private:
        std::string _manufactuer;

        int _familyId, _model, _stepping;

        bool _mmx;
        bool _sse;
        bool _sse2;
        bool _sse3;
        bool _ssse3;
        bool _sse41;
        bool _sse42;
        bool _fma;
        bool _aes;
        bool _avx;
        bool _avx2;
        bool _bmi1;
        bool _bmi2;
        bool _avx512f;
        bool _avx512dq;
        bool _avx512ifma;
        bool _avx512pf;
        bool _avx512er;
        bool _avx512cd;
        bool _avx512bw;
        bool _avx512vl;
        bool _avx512bmi;
        bool _avx512bmi2;
        bool _avx512nn;
        bool _avx512bitalg;
        bool _avx512_4nniw;
        bool _avx512_4fmaps;
        bool _avx512vp2intersect;
        bool _avx512bf16;
        bool _fp16;
        bool _sha;
        bool _amxbf16, _amxtile, _amxint8;
    };

}


#endif


