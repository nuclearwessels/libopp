

#ifndef LIBOPP_TYPE_H
#define LIBOPP_TYPE_H

#include <cstdint>


namespace libopp
{

    typedef int8_t      opp8s;
    typedef uint8_t     opp8u;
    typedef int16_t     opp16s;
    typedef uint16_t    opp16u;
    typedef uint64_t    opp64u;
    typedef int64_t     opp64s;
    typedef float       opp32f;
    typedef double      opp64f;


    enum class OppiAxis
    {
        HORIZONTAL,
        VERTICAL,
        BOTH
    };


    enum class CompareOp
    {
        LT,
        LTE,
        GT,
        GTE,
        EQUAL
    };




}


#endif

