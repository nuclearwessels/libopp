
#include "imgbase.h"
#include <cstdint>


using namespace libopp;


static const std::vector<int64_t> POWERS_TWO =
{
    1i64 << 0,
    1i64 << 1,
    1i64 << 2,
    1i64 << 3,
    1i64 << 4,
    1i64 << 5,
    1i64 << 6,
    1i64 << 7,
    1i64 << 8
};


bool libopp::IsPowerOfTwo(int64_t n)
{
    if (n > 64)
    {
        throw std::invalid_argument("Unexpected value");
    }

    auto iter = std::find(std::crbegin(POWERS_TWO), std::crend(POWERS_TWO), n);

    return iter != POWERS_TWO.crend();
}


int64_t libopp::RoundUp(int64_t n, int64_t interval)
{
    if (!IsPowerOfTwo(interval))
    {
        throw std::invalid_argument("interval is not a power of 2");
    }

    if (n % interval == 0)
    {
        return n;
    }
    else
    {
        return ((n / interval) + 1) * interval;
    }
}



int64_t libopp::RoundDown(int64_t n, int64_t interval)
{
    if (!IsPowerOfTwo(interval))
    {
        throw std::invalid_argument("interval is not power of two");
    }

    int64_t mask = interval - 1;

    return n & ~mask;
}




bool libopp::CheckCompareResult(__m256i m)
{
    for (int i = 0; i < REG256_WIDTH; i++)
    {
        if (m.m256i_i8[i] == 0)
        {
            return false;
        }
    }

    return true;
}



__m256i libopp::GetLeftMaskM256U8(int num)
{
    constexpr uint8_t MASK = 0xFF;

    if (num > REG256_WIDTH)
    {
        throw std::invalid_argument("num too large");
    }

    __m256i m = _mm256_setzero_si256();

    for (int i = 0; i < num; i++)
    {
        m.m256i_u8[i] = MASK;
    }

    return m;
}



__m256i libopp::GetLeftMaskComplementM256U8(int num)
{
    constexpr uint8_t MASK = 0xFF;

    if (num > REG256_WIDTH)
    {
        throw std::invalid_argument("num too large");
    }

    __m256i m = _mm256_setzero_si256();

    for (int i = num; i < REG256_WIDTH; i++)
    {
        m.m256i_u8[i] = MASK;
    }

    return m;
}



__m256i libopp::GetRightMaskM256U8(int num)
{
    constexpr uint8_t MASK = 0xFF;
    
    __m256i mask = _mm256_set1_epi8(MASK);

    for (int i = 0; i < num; i++)
    {
        mask.m256i_u8[i] = 0;
    }

    return mask;
}



__m256i libopp::GetRightMaskComplementM256U8(int num)
{
    constexpr uint8_t MASK = 0xFF;

    __m256i mask = _mm256_set1_epi8(MASK);

    for (int i = num; i < REG256_WIDTH; i++)
    {
        mask.m256i_u8[i] = 0;
    }

    return mask;
}

