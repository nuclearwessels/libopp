

#include "img8uc1.h"

#include "cpuinfo.h"

#include <immintrin.h>
#include <sstream>


using namespace libopp;


MinMax8u::MinMax8u() : 
    minValue(0),
    maxValue(0)
{

}


MinMax8u::MinMax8u(opp8u min, opp8u max) :
    minValue(min),
    maxValue(max)
{

}



MinMaxIndex8u::MinMaxIndex8u() :
    MinMax8u(),
    minPoint(Point::Origin),
    maxPoint(Point::Origin)
{
    
}



libopp::Img8uC1::Img8uC1() : ImgBase(1, false)
{

}


libopp::Img8uC1::Img8uC1(int64_t w, int64_t h) : 
    ImgBase(w, h, 1, false)
{  
    AllocateImage();
}


std::vector<std::string> libopp::Img8uC1::str() const
{
    std::stringstream ss;

    for (int64_t y = 0; y < height(); y++)
    {
        opp8u* p = ptr(y);

        for (int64_t x = 0; x < width(); x++, p++)
        {
            ss << *p;

            if (x != (width() - 1))
            {
                ss << ",";
            }
        }

        ss << std::endl;
    }
    
    return std::vector<std::string> { ss.str() };
}



bool libopp::Img8uC1::operator == (opp8u value) const
{
    if (CpuInfo::cpuInfo().avx2())
    {
        constexpr opp8u ONES = 0xFF;

        const __m256i ONES256 = _mm256_set1_epi8(ONES);
        const __m256i VALUE256 = _mm256_set1_epi8(value);

        ImageCmpOp256_1 op = [&](__m256i a) -> int
        {
            __m256i result = _mm256_cmpeq_epi8(a, VALUE256);

            return _mm256_testz_si256(result, ONES256);
        };

        return process_8u_256_test_srcA(rect(), op);
    }
    else
    {
        throw NotImplementedException("operator == (value)");
    }
}



void libopp::Img8uC1::set(opp8u value)
{
    set(rect(), value);
}


void Img8uC1::set(const Rect& roi, opp8u value)
{
    if (CpuInfo::cpuInfo().avx2())
    {
        __m256i value256 = _mm256_set1_epi8(value);

        ImageOp256_1 op = [&](__m256i i) -> __m256i
        {
            return value256;
        };

        process_8u_256_srcA_dstA(roi, op);
    }
    else
    {
        throw NotImplementedException("Img8uC1::set");
    }
}



Img8uC1 Img8uC1::copy() const
{
    return copy(rect());
}


Img8uC1 Img8uC1::copy(const Rect& srcRect) const
{
    if (CpuInfo::cpuInfo().avx2())
    {
        ImageOp256_1 op = [](__m256i a) -> __m256i
        {
            return a;
        };

        Img8uC1 dst(srcRect.width, srcRect.height);

        process_8u_256_srcA_dstB(srcRect, dst, Point::Origin, op);
        
        return dst;
    }
    else
    {
        throw NotImplementedException("Img8uC1::copy");
    }
}



void Img8uC1::add(opp8u value)
{
    add(rect(), value);
}

void Img8uC1::add(const Rect& roi, opp8u value)
{
    if (CpuInfo::cpuInfo().avx2())
    {
        const __m256i VALUE256 = _mm256_set1_epi8(value);

        ImageOp256_1 op = [&](__m256i v) -> __m256i
        {
            return _mm256_add_epi8(v, VALUE256);
        };

        process_8u_256_srcA_dstA(roi, op);
    }
    else
    {
        throw NotImplementedException("Img8uC1::add");
    }
}


void Img8uC1::add(const Rect& srcDstRect, const Img8uC1& src2, const Point& src2Pt)
{
    if (CpuInfo::cpuInfo().avx2())
    {
        ImageOp256_2 op = [&](__m256i a, __m256i b) -> __m256i
        {
            return _mm256_add_epi8(a, b);
        };

        process_8u_256_srcA_srcB_dstA(srcDstRect, src2, src2Pt, op);
    }
    else
    {
        throw NotImplementedException("Img8uC1::add");
    }
}


Img8uC1 Img8uC1::compare(const Rect& srcRect, const Img8uC1& src2, const Point& src2pt, CompareOp cmpOp) const
{
    __m256i v = _mm256_set1_epi8(1);

    //_mm256_testz_si256()

    if (CpuInfo::cpuInfo().avx2())
    {
        ImageOp256_2 op;

        switch (cmpOp)
        {
        case CompareOp::EQUAL:
            op = [&](__m256i a, __m256i b) -> __m256i
            {
                return _mm256_cmpeq_epi8(a, b);
            };
            break;

        case CompareOp::GT:
            op = [&](__m256i a, __m256i b) -> __m256i
            {
                return _mm256_cmpgt_epi8(a, b);
            };
            break;

        case CompareOp::GTE:
            op = [&](__m256i a, __m256i b) -> __m256i
            {
                __m256i gtResult = _mm256_cmpgt_epi8(a, b);
                __m256i eqResult = _mm256_cmpeq_epi8(a, b);
                
                return _mm256_or_si256(a, b);
            };
            break;

        case CompareOp::LT:
            op = [&](__m256i a, __m256i b) -> __m256i
            {
                return _mm256_cmpgt_epi8(a, b);
            };

            break;

        case CompareOp::LTE:
            op = [&](__m256i a, __m256i b) -> __m256i
            {
                __m256i gtResult = _mm256_cmpgt_epi8(a, b);
                __m256i eqResult = _mm256_cmpeq_epi8(a, b);

                return _mm256_or_si256(a, b);
            };
            break;
        }

        Img8uC1 dst(srcRect.width, srcRect.height);

        process_8u_256_srcA_srcB_dstC(*this, srcRect, src2, src2pt, dst, Point::Origin, op);
    }
    else
    {
        throw NotImplementedException("No implementation for compare found.");
    }
}



void Img8uC1::process_8u_256_srcA_dstA(const Rect& roi, const ImageOp256_1& op)
{
    int64_t registerCount = roi.width / REG256_WIDTH;
    int64_t registerRemainderCount = roi.width - registerCount * REG256_WIDTH;

    __m256i rightMask = GetRightMaskM256U8((int) registerRemainderCount);
    __m256i rightMaskComplement = GetRightMaskComplementM256U8((int)registerRemainderCount);


    for (int64_t y = roi.y; y < (roi.y + roi.height); y++)
    {
        __m256i* p = reinterpret_cast<__m256i*>( ptr(roi.x, y) );

        // perform the whole register operations first
        for (int64_t registerX = 0; registerX < registerCount; registerX++, p++)
        {
            __m256i value = *p;
            __m256i valueOp = op(value);
            *p = valueOp;
        }

        // now handle the remainder
        if (registerRemainderCount)
        {
            __m256i value = *p;

            __m256i valueOp = op(value);

            __m256i newBits = _mm256_and_si256(rightMaskComplement, valueOp);
            __m256i oldBits = _mm256_and_si256(rightMask, value);

            __m256i newValue = _mm256_or_si256(newBits, oldBits);
            
            *p = newValue;
        }

    }
}


//! @brief Performs an operation, returning immediately if the operation is non-zero.
//! @param roi region of interest
//! @param op the operation to perform
//! @return false if the images are equal
bool Img8uC1::process_8u_256_test_srcA(const Rect& roi, const ImageCmpOp256_1& op) const
{
    int64_t registerCount = roi.width / REG256_WIDTH;
    int64_t registerRemainderCount = roi.width - registerCount * REG256_WIDTH;

    __m256i rightMask = GetRightMaskM256U8((int)registerRemainderCount);
    __m256i rightMaskComplement = GetRightMaskComplementM256U8((int)registerRemainderCount);


    for (int64_t y = roi.y; y < (roi.y + roi.height); y++)
    {
        __m256i* p = reinterpret_cast<__m256i*>(ptr(roi.x, y));

        // perform the whole register operations first
        for (int64_t registerX = 0; registerX < registerCount; registerX++, p++)
        {
            __m256i value = _mm256_loadu_si256(p);
            int result = op(value);
            if (result)
            {
                return false;
            }
        }

        // now handle the remainder
        if (registerRemainderCount)
        {
            __m256i value = _mm256_loadu_si256(p);
            __m256i newBits = _mm256_and_si256(rightMaskComplement, value);
            //__m256i oldBits = _mm256_and_si256(rightMask, value);
            //__m256i newValue = _mm256_or_si256(newBits, oldBits);

            int result = op(newBits);
            if (result)
            {
                return false;
            }
        }

    }

    return true;
}


void Img8uC1::process_8u_256_srcA_dstB(const Rect& srcRoi, Img8uC1& dst, const Point& dstPt, const ImageOp256_1& op) const
{
    int64_t registerCount = srcRoi.width / REG256_WIDTH;
    int64_t registerRemainderCount = srcRoi.width - registerCount * REG256_WIDTH;

    __m256i rightMask = GetRightMaskM256U8((int)registerRemainderCount);
    __m256i rightMaskComplement = GetRightMaskComplementM256U8((int)registerRemainderCount);

    dst.validateROI(Rect(dstPt, srcRoi));

    for (int64_t y = 0; y < srcRoi.height; y++)
    {
        int64_t ySrc = srcRoi.y + y;
        int64_t yDst = dstPt.y + y;

        __m256i* pSrc = reinterpret_cast<__m256i*>(ptr(srcRoi.x, ySrc));
        __m256i* pDst = reinterpret_cast<__m256i*>(dst.ptr(dstPt.x, yDst));

        // perform the whole register operations first
        for (int64_t registerX = 0; registerX < registerCount; registerX++, pSrc++, pDst++)
        {
            __m256i value = *pSrc;
            __m256i valueOp = op(value);
            *pDst = valueOp;
        }

        // now handle the remainder
        if (registerRemainderCount)
        {
            __m256i value = *pSrc;

            __m256i valueOp = op(value);

            __m256i newBits = _mm256_and_si256(rightMaskComplement, valueOp);
            __m256i oldBits = _mm256_and_si256(rightMask, value);

            __m256i newValue = _mm256_or_si256(newBits, oldBits);

            *pDst = newValue;
        }
    }

}



//! @brief Performs an operation between two images and stores the result in the first.
//! @param roiA region of image A
//! @param imgB image B
//! @param ptB point of image B
//! @param op the operation to be performed
void Img8uC1::process_8u_256_srcA_srcB_dstA(const Rect& roiA, const Img8uC1& imgB, const Point& ptB, const ImageOp256_2& op)
{
    Rect rectB(ptB, roiA);

    imgB.validateROI(rectB);

    int64_t registerCount = roiA.width / REG256_WIDTH;
    int64_t registerRemainderCount = roiA.width - registerCount * REG256_WIDTH;

    __m256i rightMask = GetRightMaskM256U8((int)registerRemainderCount);
    __m256i rightMaskComplement = GetRightMaskComplementM256U8((int)registerRemainderCount);

    for (int64_t y = 0; y < roiA.height; y++)
    {
        int64_t yA = roiA.y + y;
        int64_t yB = ptB.y + y;

        __m256i* pA = reinterpret_cast<__m256i*>(ptr(roiA.x, yA));
        __m256i* pB = reinterpret_cast<__m256i*>(imgB.ptr(ptB.x, yB));

        for (int64_t registerX = 0; registerX < registerCount; registerX++, pA++, pB++)
        {
            __m256i valueA = *pA;
            __m256i valueB = *pB;

            *pA = op(valueA, valueB);
        }

        if (registerRemainderCount)
        {
            __m256i valueA = *pA;
            __m256i valueB = *pB;

            __m256i valueOp = op(valueA, valueB);

            __m256i newBits = _mm256_and_si256(rightMaskComplement, valueOp);
            __m256i oldBits = _mm256_and_si256(rightMask, valueA);

            __m256i newValue = _mm256_or_si256(newBits, oldBits);

            *pA = newValue;
        }
    }
}




//! @brief Performs an operation between two images and stores the result in a third.
//! @param roiA region of image A
//! @param imgB image B
//! @param ptB point of image B
//! @param op the operation to be performed
void Img8uC1::process_8u_256_srcA_srcB_dstC(const Img8uC1& imgA, const Rect& roiA, const Img8uC1& imgB, const Point& ptB, 
    Img8uC1& dst, const Point& dstPt,
    const ImageOp256_2& op) const
{
    imgA.validateROI(roiA);
    imgB.validateROI(Rect(ptB, roiA));
    dst.validateROI(Rect(dstPt, roiA));

    int64_t registerCount = roiA.width / REG256_WIDTH;
    int64_t registerRemainderCount = roiA.width - registerCount * REG256_WIDTH;

    __m256i rightMask = GetRightMaskM256U8((int)registerRemainderCount);
    __m256i rightMaskComplement = GetRightMaskComplementM256U8((int)registerRemainderCount);

    for (int64_t y = 0; y < roiA.height; y++)
    {
        int64_t yA = roiA.y + y;
        int64_t yB = ptB.y + y;
        int64_t yDst = dstPt.y + y;

        __m256i* pA = reinterpret_cast<__m256i*>(imgA.ptr(roiA.x, yA));
        __m256i* pB = reinterpret_cast<__m256i*>(imgB.ptr(ptB.x, yB));
        __m256i* pDst = reinterpret_cast<__m256i*>(dst.ptr(dstPt.x, yDst));

        for (int64_t registerX = 0; registerX < registerCount; registerX++, pA++, pB++, pDst++)
        {
            __m256i valueA = *pA;
            __m256i valueB = *pB;

            *pDst = op(valueA, valueB);
        }

        if (registerRemainderCount)
        {
            __m256i valueA = *pA;
            __m256i valueB = *pB;

            __m256i valueOp = op(valueA, valueB);

            __m256i newBits = _mm256_and_si256(rightMaskComplement, valueOp);
            __m256i oldBits = _mm256_and_si256(rightMask, valueA);

            __m256i newValue = _mm256_or_si256(newBits, oldBits);

            *pDst = newValue;
        }
    }
}
