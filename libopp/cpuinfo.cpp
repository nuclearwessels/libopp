
#include "cpuinfo.h"

#include <intrin.h>

#include <array>
#include <bitset>
#include <iterator>

using namespace libopp;


const CpuInfo CpuInfo::_cpuInfo = CpuInfo();


libopp::CpuInfo::CpuInfo() :
    _mmx(false),
    _sse(false),
    _sse2(false),
    _sse3(false),
    _ssse3(false),
    _sse41(false),
    _sse42(false),
    _fma(false),
    _aes(false),
    _avx(false),
    _avx2(false),
    _bmi1(false),
    _bmi2(false),
    _avx512f(false),
    _avx512dq(false),
    _avx512ifma(false),
    _avx512pf(false),
    _avx512er(false),
    _avx512cd(false),
    _avx512bw(false),
    _avx512vl(false),
    _avx512bmi(false),
    _avx512bmi2(false),
    _avx512nn(false),
    _avx512bitalg(false),
    _avx512_4nniw(false),
    _avx512_4fmaps(false),
    _avx512vp2intersect(false),
    _avx512bf16(false),
    _fp16(false),
    _sha(false),
    _amxbf16(false),
    _amxtile(false),
    _amxint8(false)
{
    std::vector<unsigned int> v;


    v = GetCpuInfo(0);

    int highestInfo = v.at(0);

    constexpr std::array<int, 3> ORDER = { 1, 3, 2 };

    for (int i : ORDER)
    {
        int c = v.at(i);

        for (int j = 0; j < 4; j++)
        {
            _manufactuer.push_back((char)(c & 0x000000FF));
            c >>= 8;
        }
    }


    std::bitset<32> eax, ebx, ecx, edx;

    v = GetCpuInfo(1);

    _familyId = GetBits(v.at(0), 8, 11);
    _model = GetBits(v.at(0), 4, 7);
    _stepping = GetBits(v.at(0), 0, 3);

    edx = v.at(3);
    _mmx = edx.test(23);
    _sse = edx.test(25);
    _sse2 = edx.test(26);

    ecx = v.at(2);
    _sse3 = ecx.test(0);
    _ssse3 = ecx.test(9);
    _fma = ecx.test(12);
    _sse41 = ecx.test(19);
    _sse42 = ecx.test(20);
    _aes = ecx.test(25);
    _avx = ecx.test(28);

    _fp16 = ecx.test(29);


    v = GetCpuInfo(7);

    ebx = v.at(1);
    _bmi1 = ebx.test(3);
    _avx2 = ebx.test(5);
    _bmi2 = ebx.test(8);

    _avx512f = ebx.test(16);
    _avx512dq = ebx.test(17);
    _avx512ifma = ebx.test(21);
    _avx512pf = ebx.test(26);
    _avx512er = ebx.test(27);
    _avx512cd = ebx.test(28);

    _sha = ebx.test(29);
    _avx512bw = ebx.test(30);
    _avx512vl = ebx.test(31);

    ecx = v.at(2);
    _avx512bmi = ecx.test(1);
    _avx512bmi2 = ecx.test(6);
    _avx512nn = ecx.test(11);
    _avx512bitalg = ecx.test(12);

    edx = v.at(3);
    _avx512_4nniw = edx.test(2);
    _avx512_4fmaps = edx.test(3);
    _avx512vp2intersect = edx.test(8);

    _amxbf16 = edx.test(22);
    _amxtile = edx.test(23);
    _amxint8 = edx.test(24);

    v = GetCpuInfo(7, 1);
    eax = v.at(0);
    _avx512bf16 = eax.test(5);
}



bool CpuInfo::mmx() const
{
    return _mmx;
}

bool CpuInfo::sse() const
{
    return _sse;
}

bool CpuInfo::sse2() const
{
    return _sse2;
}

bool CpuInfo::sse3() const
{
    return _sse3;
}


bool CpuInfo::ssse3() const
{
    return _ssse3;
}

bool CpuInfo::fma() const
{
    return _fma;
}

bool CpuInfo::sse41() const
{
    return _sse41;
}

bool CpuInfo::sse42() const
{
    return _sse42;
}

bool CpuInfo::aes() const
{
    return _aes;
}

bool CpuInfo::avx() const
{
    return _avx;
}

bool CpuInfo::avx2() const
{
    return _avx2;
}

bool CpuInfo::bmi1() const
{
    return _bmi1;
}

bool CpuInfo::bmi2() const
{
    return _bmi2;
}

bool CpuInfo::avx512f() const
{
    return _avx512f;
}

bool CpuInfo::avx512dq() const
{
    return _avx512dq;
}

bool CpuInfo::avx512ifma() const
{
    return _avx512ifma;
}

bool CpuInfo::avx512pf() const
{
    return _avx512pf;
}

bool CpuInfo::avx512er() const
{
    return _avx512er;
}

bool CpuInfo::avx512cd() const
{
    return _avx512cd;
}

bool CpuInfo::avx512bw() const
{
    return _avx512bw;
}

bool CpuInfo::avx512vl() const
{
    return _avx512vl;
}

bool CpuInfo::avx512bmi() const
{
    return _avx512bmi;
}

bool CpuInfo::avx512bmi2() const
{
    return _avx512bmi2;
}

bool CpuInfo::avx512nn() const
{
    return _avx512nn;
}

bool CpuInfo::avx512bitalg() const
{
    return _avx512bitalg;
}

bool CpuInfo::avx512_4nniw() const
{
    return _avx512_4nniw;
}

bool CpuInfo::avx512_4fmaps() const
{
    return _avx512_4fmaps;
}

bool CpuInfo::avx512vp2intersect() const
{
    return _avx512vp2intersect;
}

bool CpuInfo::fp16() const
{
    return _fp16;
}



const CpuInfo& CpuInfo::cpuInfo()
{
    return _cpuInfo;
}


int CpuInfo::GetBits(int d, int startPos, int endPos)
{
    std::bitset<32> b = d;
    std::bitset<32> v;

    for (int i = startPos, vi = 0; i <= endPos; i++, vi++)
    {
        v.set(vi, b.test(i));
    }

    return (int)v.to_ulong();
}



std::vector<unsigned int> libopp::CpuInfo::GetCpuInfo(unsigned int infoType, unsigned int subFeature)
{
    std::array<int, 4> regs;

#ifdef _MSC_VER
    if (subFeature)
    {
        __cpuidex(regs.data(), infoType, subFeature);
    }
    else
    {
        __cpuid(regs.data(), infoType);
    }

#else
#error cpuid not implemented

#endif

    std::vector<unsigned int> v;

    std::copy(std::cbegin(regs), std::cend(regs), std::back_inserter(v));

    return v;
}

