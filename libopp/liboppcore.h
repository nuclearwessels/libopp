
#pragma once

#include <exception>
#include <initializer_list>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cstdint>
#include <optional>

#include "libopptype.h"


namespace libopp
{

    struct OppImgDeleter
    {
        void operator()(void* buf);
    };


    class OppException : public std::exception
    {
    public:
        OppException();
        OppException(const std::string& msg);
        const char* what() const;

    protected:
        OppException(const std::string& exceptionName, const std::string& msg);

        std::string _exceptionName;
        std::string _msg;
    };


    class NotImplementedException : public OppException
    {
    public:
        NotImplementedException();
        NotImplementedException(const std::string& msg);
    };

    struct Size
    {
        Size();
        Size(int64_t w, int64_t h);
        Size(const std::initializer_list<int64_t>& init);
        
        bool operator == (const Size& s) const;
        bool operator != (const Size& s) const;

        int64_t width, height;
    };


    struct Point
    {
        Point();
        Point(int64_t x, int64_t y);
        Point(const std::initializer_list<int64_t>& init);
        
        bool operator == (const Point& pt) const;
        bool operator != (const Point& pt) const;

        int64_t x, y;

        static const Point Origin;
    };


    struct Rect : Size, Point
    {
        Rect();
        Rect(int64_t x, int64_t y, int64_t w, int64_t h);
        Rect(const Point& pt, const Size& size);

        bool operator == (const Rect& a) const;
        bool operator != (const Rect& a) const;

        Point upperLeft() const;
        Point upperRight() const;
        Point lowerRight() const;
        Point lowerLeft() const;

        //! @brief Test this rectangle is within rectangle r
        //! @param r the exterior rectance
        //! @return true if this rectangle is within rectangle r, false if not
        bool inside(const Rect& r) const;

        

    };
    
    

    struct MeanStdDev
    {
        MeanStdDev();
        MeanStdDev(double mean, double stdDev);

        double mean;
        double stdDev;
    };






    void  Validate(int expr);

    template<class T>
    void ValidatePtr(T* ptr)
    {
        if (!ptr)
            throw OppException("Invalid ptr");
    }


    template<class T>
    void ValidateMalloc(T* ptr)
    {
        if (!ptr)
            throw OppException("Malloc failed.");
    }



    constexpr int REG256_WIDTH = 256 / 8;


}


