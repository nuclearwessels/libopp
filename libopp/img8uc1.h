


#ifndef LIBOPP_IMG8UC1_H
#define LIBOPP_IMG8UC1_H

#include "imgbase.h"



namespace libopp
{

struct MinMax8u
{
    MinMax8u();
    MinMax8u(opp8u min, opp8u max);

    opp8u minValue;
    opp8u maxValue;
};


struct MinMaxIndex8u : MinMax8u
{
    MinMaxIndex8u();

    Point minPoint;
    Point maxPoint;
};



class Img8uC1 : public ImgBase<opp8u>
{
public:
    Img8uC1();
    Img8uC1(int64_t w, int64_t h);

    std::vector<std::string> str() const override;

    bool operator == (opp8u value) const;
    bool operator == (const Img8uC1& src2) const;


    void set(opp8u value);
    void set(const Rect& roi, opp8u value);
    

    //Img16uC1 convert16uC1() const;
    //Img16uC1 convert16uC1(const Rect& srcRect) const;
    //
    //Img16sC1 convert16sC1() const;
    //Img16sC1 convert16sC1(const Rect& srcRect) const;   

    //Img32fC1 convert32fC1() const;
    //Img32fC1 convert32fC1(const Rect& srcRect) const;
    

    Img8uC1 copy() const;
    Img8uC1 copy(const Rect& rect) const;
    


    //void copy(const Rect& srcRect, Img8uC1& dst, const Point& dstPt) const;
    //void copyMask(const Rect& srcRect, Img8uC1& dst, const Point& dstPt, const Img8uC1& mask, const Point& maskPt) const;
    
    //void copyC3(const Rect& srcRect, Img8uC3& dst, const Point& dstPt) const;

	//void dupC3(const Point& pt, Ipp8uC3& dst, const IppiPoint& dstPt, const IppiSize& size) const;
	//void dupC4(const Point& pt, Ipp8uC4& dst, const IppiPoint& dstPt, const IppiSize& size) const;

	//void transpose(const Rect& roi);
	//void transpose(const Point& srcPt, Ipp8uC1& dst, const IppiPoint& dstPt, const IppiSize& size) const;

    //void mirror(IppiAxis axis);
    //void mirror(IppiAxis axis, const Rect& roi);


    //! Adds a constant to the specified ROI.
    void add(opp8u value);
    void add(const Rect& rect, opp8u value);

    //! Add a second image to this image.
    void add(const Rect& srcDstRect, const Img8uC1& src2, const Point& src2Pt);

    
    void and_(const Img8uC1& src);
    
    void and_(const Rect& srcDstRect, const Img8uC1& src, const Point& srcPt);
	
    void and_(const Point& srcDstPt, const Img8uC1& src, const Rect& srcRect);


    //! ippiAndC_8u_C1IR
    void andC(opp8u value, const Rect& srcDstRect);

    
    Img8uC1 compare(const Rect& srcRect, const Img8uC1& src2, const Point& src2pt, CompareOp cmpOp = CompareOp::EQUAL) const;
    Img8uC1 compare(const Img8uC1& src2, const Point& src2pt, CompareOp cmpOp = CompareOp::EQUAL) const;
    Img8uC1 compare(const Img8uC1& src2, CompareOp cmpOp = CompareOp::EQUAL) const;

    Img8uC1 compare(opp8u v, CompareOp cmpOp) const;
    Img8uC1 compare(const Rect& rect, opp8u v, CompareOp cmpOp) const;
  
    int countInRange(opp8u lowerBound, opp8u upperBound) const;
    int countInRange(const Rect& rect, opp8u lowerBound, opp8u upperBound) const;


    void or_ (const Rect& srcDstRect, const Img8uC1& src, const Img8uC1& srcPt);
    
    void xor_(const Rect& srcDstRect, const Img8uC1& src, const Img8uC1& srcPt);
    Img8uC1 xor_(const Img8uC1& src2) const;
    
    void not_(const Rect& srcDstRect);

    void andC(const Rect& srcDstRect, opp8u value);
    
    void orC(const Rect& srcDstRect, opp8u value);

    Img8uC1 xorC(opp8u value) const;
    void xorC(const Rect& roi, opp8u value);

    opp8u min_() const;
    opp8u min_(const Rect& rect) const;
    
    opp8u max_() const;
    opp8u max_(const Rect& rect) const;

    std::vector<MinMax8u> minMax() const;
    std::vector<MinMax8u> minMax(const Rect& rect) const;

    std::vector<MinMaxIndex8u> minMaxIndex() const;
    std::vector<MinMaxIndex8u> minMaxIndex(const Rect& rect) const;

    opp64f mean() const;
    opp64f mean(const Rect& rect) const;

    MeanStdDev meanStdDev() const;
    MeanStdDev meanStdDev(const Rect& rect) const;
    MeanStdDev meanStdDev(const Rect& rect, const Img8uC1& mask, const Point& maskPt) const;

    opp64s sum() const;
    opp64s sum(const Rect& rect) const;


protected:
    bool process_8u_256_test_srcA(const Rect& roi, const ImageCmpOp256_1& op) const;

    void process_8u_256_srcA_dstA(const Rect& roi, const ImageOp256_1& op);
    void process_8u_256_srcA_dstB(const Rect& srcRoi, Img8uC1& dst, const Point& dstPt, const ImageOp256_1& op) const;
    void process_8u_256_srcA_srcB_dstA(const Rect& roiA, const Img8uC1& imgB, const Point& ptB, const ImageOp256_2& op);
    void process_8u_256_srcA_srcB_dstC(const Img8uC1& imgA, const Rect& roiA, const Img8uC1& imgB, const Point& ptB, 
        Img8uC1& dst, const Point& dstPt,
        const ImageOp256_2& op) const;
};


}


#endif
