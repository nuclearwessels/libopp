

#ifndef LIBOPP_IMG_BASE_H
#define LIBOPP_IMG_BASE_H

#include "liboppcore.h"

#include <initializer_list>

#include <immintrin.h>
#include <functional>

namespace libopp
{
    bool IsPowerOfTwo(int64_t n);

    int64_t RoundUp(int64_t n, int64_t interval);
    int64_t RoundDown(int64_t x, int64_t interval);

    __m256i GetLeftMaskM256U8(int num);
    __m256i GetLeftMaskComplementM256U8(int num);

    __m256i GetRightMaskM256U8(int num);
    __m256i GetRightMaskComplementM256U8(int num);

    bool CheckCompareResult(__m256i m);

    template<class T>
    class ImgBase
    {
    public:
        ImgBase(int numPlanesOrChannels, bool planar = false);
        ImgBase(int64_t w, int64_t h, int numPlanesOrChannels, bool planar = false);

        ImgBase(const ImgBase<T>& other) = default;
        ImgBase(ImgBase<T>&& other) = default;
        ImgBase<T>& operator = (const ImgBase<T>& other) = default;
        ImgBase<T>& operator = (ImgBase<T>&& other) = default;

        virtual ~ImgBase();
        
        void writeCSV(const std::string& fileNameStem) const;

        int64_t width() const;
        int64_t height() const;
        int64_t step() const;
        int channels() const;
        int planes() const;

        Size size() const;
        Rect rect() const;

        T* ptr() const;
        T* ptr(int64_t y) const;
        T* ptr(int64_t x, int64_t y) const;
        virtual T* ptr(int64_t x, int64_t y, int plane) const;
        T* ptr(const Point& pt) const;

        void init(const std::initializer_list<T>& initValues);

        void validateXY(int64_t x, int64_t y) const;
        void validateROI(const Rect& rect) const;

        void reset();

        virtual std::vector<std::string> str() const abstract;

        

    protected:
        std::shared_ptr<T> _buffer;
        int64_t _width, _height, _step;
        int _planes, _channels;
        
        static const int64_t DEFAULT_ALIGNMENT = 64;

        void AllocateImage();

    };


    template<class T> ImgBase<T>::ImgBase(int numPlanesOrChannels, bool planar) :
        _width(0),
        _height(0),
        _step(0),
        _planes(0),
        _channels(0)
    {
        if (planar)
            _planes = numPlanesOrChannels;
        else
            _channels = numPlanesOrChannels;
    }


    template<class T>
    ImgBase<T>::ImgBase(int64_t w, int64_t h, int numPlanesOrChannels, bool planar) :
        _width(w),
        _height(h),
        _channels(0),
        _planes(0)
    {
        Validate(w > 0);
        Validate(h > 0);
        Validate(numPlanesOrChannels > 0);

        if (planar)
        {
            _planes = numPlanesOrChannels;
        }
        else
        {
            _channels = numPlanesOrChannels;
        }


    }


    template <class T>
    ImgBase<T>::~ImgBase()
    {
    }


    template<class T>
    void ImgBase<T>::init(const std::initializer_list<T>& initValues)
    {
        if (initValues.size() != _width * _height)
        {
            std::stringstream ss;
            ss << "Invalid number of values: Expected " << _width * _height << ", received " << initValues.size();
            throw OppException(ss.str().c_str());
        }

        auto i = initValues.begin();
        for (int y = 0; y < _height; y++)
        {
            T* p = ptr(0, y);
            for (int x = 0; x < _width; x++, p++, i++)
            {
                *p = *i;
            }
        }
    }


    template<class T>
    T* ImgBase<T>::ptr() const
    {
        return (T*)_buffer.get();
    }

    template<class T>
    T* ImgBase<T>::ptr(int64_t y) const
    {
        return ptr(0, y);
    }


    template<class T>
    T* ImgBase<T>::ptr(int64_t x, int64_t y) const
    {
        return ptr(x, y, 0);
    }


    template<class T>
    T* ImgBase<T>::ptr(int64_t x, int64_t y, int planes) const
    {
        ValidatePtr<T>(_buffer.get());

        validateXY(x, y);

        if (channels())
        {
            return ((T*)((opp8u*)_buffer.get() + y * step())) + x * channels();
        }
        else
        {
            throw OppException("Not implemented");
        }
    }


    template<class T>
    T* ImgBase<T>::ptr(const Point& pt) const
    {
        return ptr(pt.x, pt.y);
    }


    
    template<class T>
    int64_t ImgBase<T>::step() const
    {
        return _step;
    }


    template<class T>
    int64_t ImgBase<T>::width() const
    {
        return _width;
    }


    template<class T>
    int64_t ImgBase<T>::height() const
    {
        return _height;
    }


    template<class T>
    int ImgBase<T>::planes() const
    {
        return _planes;
    }


    template<class T>
    int ImgBase<T>::channels() const
    {
        return _channels;
    }


    template<class T>
    Size ImgBase<T>::size() const
    {
        return Size(_width, _height);
    }


    template<class T>
    Rect ImgBase<T>::rect() const
    {
        return Rect(0, 0, _width, _height);
    }

    template<class T>
    void ImgBase<T>::validateXY(int64_t x, int64_t y) const
    {
        if (x >= width())
            throw OppException("Exceeds width");

        if (y >= height())
            throw OppException("Exceeds height");
    }


    template<class T>
    void ImgBase<T>::validateROI(const Rect& roi) const
    {
        if (roi.x + roi.width > width())
            throw OppException("ROI exceeds width");

        if (roi.y + roi.height > height())
            throw OppException("ROI exceeds height");
    }


    template <class T>
    void ImgBase<T>::reset()
    {
        _buffer = std::shared_ptr<T>();
        _width = _height = _step = 0;
    }


    template <class T>
    void ImgBase<T>::AllocateImage()
    {
        if (channels())
        {
            int64_t rowBytes = width() * channels() * sizeof(T);
            _step = RoundUp(rowBytes, DEFAULT_ALIGNMENT);
            int64_t bytes = _step * height();

            T* buf = (T*)_aligned_malloc( (size_t) bytes, DEFAULT_ALIGNMENT);

            if (buf == nullptr)
            {
                throw OppException("Unable to allocate memory");
            }

            _buffer = std::shared_ptr<T>(buf, OppImgDeleter());
        }
        else
        {
            // planar layout
            throw OppException("Not implemented");
        }        
    }


    template<typename T>
    std::string ImageToStr(const T* img, int step, int w, int h, int planes, const char* format)
    {
        std::stringstream ss;

        char buf[100];

        for (int y = 0; y < h; y++)
        {
            const char* p = (const char*)img;
            p += step * y;

            T* row = (T*)(p);

            for (int x = 0; x < w; x++)
            {
                if (planes > 1)
                    ss << "(";

                for (int plane = 0; plane < planes; plane++, p++)
                {
                    T value = *p;
                    snprintf(buf, 100, format, value);

                    ss << buf;

                    if (plane + 1 < planes)
                        ss << ",";
                }

                if (planes > 1)
                    ss << ")";

                ss << " ";
            }

            ss << std::endl;
        }

        return ss.str();
    }


    template<class T, typename PT>
    std::string ImageToStr(const T& img, const char* format)
    {
        return ImageToStr(img.ptr(), img.step(), img.width(), img.height(), img.planes(), format);
    }


    typedef std::function< int(__m256i a) > ImageCmpOp256_1;
    typedef std::function< int(__m256i a, __m256i b) > ImageCmpOp256_2;

    typedef std::function< __m256i (__m256i value) > ImageOp256_1;
    typedef std::function< __m256i (__m256i value, __m256i a) > ImageOp256_2;
    typedef std::function< __m256i (__m256i value, __m256i a, __m256i b) >  ImageOp256_3;
    

}

#endif


