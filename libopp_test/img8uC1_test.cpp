


#include "catch.hpp"

#include "img8uc1.h"

using namespace libopp;

#ifdef _WIN32
#include <Windows.h>
#include <debugapi.h>
#endif


void InitTestImg(Img8uC1& img)
{
    for (int64_t y = 0; y < img.size().height; y++)
    {
        opp8u* p = img.ptr(y);

        for (int64_t x = 0; x < img.size().width; x++, p++)
        {
            int64_t value64 = y + x;
            opp8u value = (opp8u)(value64 & 0xFF);

            *p = value;
        }
    }
}



TEST_CASE("img8uc1_alloc", "[img8uc1]")
{
    Img8uC1 img(1000, 1000);


}




TEST_CASE("img8uc1_set", "[img8uc1]")
{
    Img8uC1 img(1000, 1000);

    img.set(0);

    for (int y = 0; y < img.height(); y++)
    {
        opp8u* p = img.ptr(y);

        for (int x = 0; x < img.width(); x++, p++)
        {
            REQUIRE(*p == 0);
        }
    }
}



TEST_CASE("img8uc1_set_roi", "[img8uc1]")
{
    const Rect RECT(20, 20, 60, 60);

    constexpr int HEIGHT = 100;
    constexpr int WIDTH = 100;

    Img8uC1 img(WIDTH, HEIGHT);

    img.set(2);

    img.set(RECT, 1);

    for (int y = 0; y < HEIGHT; y++)
    {
        opp8u* p = img.ptr(y);

        uint8_t bytes[WIDTH];
        memcpy(bytes, p, WIDTH);

        for (int x = 0; x < WIDTH; x++, p++)
        {
            if (x >= RECT.x &&
                x <  (RECT.x + RECT.width) &&
                y >= RECT.y &&
                y <  (RECT.y + RECT.height) )
            {
                REQUIRE(*p == 1);
            }
            else
            {
                REQUIRE(*p == 2);
            }
        }
    }
}


TEST_CASE("img8uc1_add_c", "[img8uc1]")
{
    constexpr int HEIGHT = 100;
    constexpr int WIDTH = 100;

    constexpr opp8u VALUE_A = 1;
    constexpr opp8u VALUE_B = 2;

    Img8uC1 imgA(WIDTH, HEIGHT);
    imgA.set(imgA.rect(), VALUE_A);


    imgA.add(imgA.rect(), VALUE_B);

    for (int y = 0; y < HEIGHT; y++)
    {
        opp8u* p = imgA.ptr(y);

        for (int x = 0; x < WIDTH; x++, p++)
        {
            REQUIRE( *p == (VALUE_A + VALUE_B));
        }
    }

}



TEST_CASE("img8uc1_add_2_imgs", "[img8uc1]")
{
    constexpr int HEIGHT = 100;
    constexpr int WIDTH = 100;

    constexpr opp8u VALUE_A = 1;
    constexpr opp8u VALUE_B = 2;

    Img8uC1 imgA(WIDTH, HEIGHT);
    imgA.set(imgA.rect(), VALUE_A);
    
    Img8uC1 imgB(WIDTH, HEIGHT);
    imgB.set(imgB.rect(), VALUE_B);

    imgA.add(imgA.rect(), imgB, Point::Origin);

    for (int y = 0; y < HEIGHT; y++)
    {
        opp8u* pA = imgA.ptr(y);
        opp8u* pB = imgB.ptr(y);

        for (int x = 0; x < WIDTH; x++, pA++, pB++)
        {
            REQUIRE( *pA == (VALUE_A + VALUE_B));
        }
    }

}



TEST_CASE("img8uc1_equal", "[img8uc1]")
{
    Img8uC1 img(50, 50);
    img.set(1);

    bool equal = img == 1;
    REQUIRE(equal);
}



TEST_CASE("img8uc1_copy", "[img8uc1]")
{
    Img8uC1 imgA(50, 50);

    InitTestImg(imgA);

    constexpr int COPY_HEIGHT = 48;
    constexpr int COPY_WIDTH = 48;
    constexpr int OFFSET_X = 1;
    constexpr int OFFSET_Y = 1;

    Rect rectB = Rect(OFFSET_X, OFFSET_Y, COPY_WIDTH, COPY_HEIGHT);
    
    Img8uC1 imgB = imgA.copy(rectB);

    for (int y = 0; y < COPY_HEIGHT; y++)
    {
        opp8u* pA = imgA.ptr(OFFSET_X, OFFSET_Y + y);
        opp8u* pB = imgB.ptr(y);

        for (int x = 0; x < COPY_WIDTH; x++, pA++, pB++)
        {
            REQUIRE(*pA == *pB);
        }
    }

}