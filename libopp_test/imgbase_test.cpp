
#include "catch.hpp"

#include "imgbase.h"

using namespace libopp;


TEST_CASE("IsPowerOfTwo", "[imgbase]")
{
    bool is2 = IsPowerOfTwo(33);
    REQUIRE_FALSE(is2);

    is2 = IsPowerOfTwo(64);
    REQUIRE(is2);
}


TEST_CASE("RoundUp", "[imgbase]")
{
    int64_t r = RoundUp(33, 64);
    REQUIRE(r == 64);

}


TEST_CASE("RoundDown", "[imgbase]")
{
    int64_t r = RoundDown(53, 64);
    REQUIRE(r == 0);

    r = RoundDown(53, 32);
    REQUIRE(r == 32);
}




TEST_CASE("CheckCompareResult", "[imgbase]")
{
    constexpr int WIDTH = 256 / 8;
    
    __m256i result = _mm256_setzero_si256();
    
    bool equal = CheckCompareResult(result);
    REQUIRE_FALSE(equal);

    for (int i = 0; i < WIDTH; i++)
    {
        result = _mm256_setzero_si256();
        result.m256i_u8[i] = 0xFF;
        
        equal = CheckCompareResult(result);
        REQUIRE_FALSE(equal);
    }

    __m256i result2 = _mm256_setzero_si256();
    
    for (int i = 0; i < WIDTH; i++)
    {
        result2.m256i_u8[i] = 0xFF;
    }

    equal = CheckCompareResult(result2);
    REQUIRE(equal);

}




TEST_CASE("GetLeftMaskM256U8", "[imgbase]")
{
    for (int i = 0; i < REG256_WIDTH; i++)
    {
        __m256i correctValue = _mm256_setzero_si256();

        for (int j = 0; j < i; j++)
        {
            correctValue.m256i_u8[j] = 0xFF;
        }

        __m256i m = GetLeftMaskM256U8(i);

        __m256i res = _mm256_cmpeq_epi8(correctValue, m);

        REQUIRE(CheckCompareResult(res));
    }
}



TEST_CASE("GetLeftMaskComplementM256U8", "[imgbase]")
{
    for (int i = 0; i < REG256_WIDTH; i++)
    {
        __m256i correctValue = _mm256_setzero_si256();

        for (int j = i; j < REG256_WIDTH; j++)
        {
            correctValue.m256i_u8[j] = 0xFF;
        }

        __m256i m = GetLeftMaskComplementM256U8(i);

        __m256i res = _mm256_cmpeq_epi8(correctValue, m);

        REQUIRE(CheckCompareResult(res));
    }
}



TEST_CASE("GetRightMaskM256U8", "[imgbase]")
{
    constexpr uint8_t MASK = 0xFF;

    for (int i = 0; i < REG256_WIDTH; i++)
    {
        __m256i correctValue = _mm256_set1_epi8(MASK);

        for (int j = 0; j < i; j++)
        {
            correctValue.m256i_u8[j] = 0x00;
        }

        __m256i m = GetRightMaskM256U8(i);

        __m256i res = _mm256_cmpeq_epi8(correctValue, m);

        REQUIRE(CheckCompareResult(res));
    }
}



TEST_CASE("GetRightMaskComplementM256U8", "[imgbase]")
{
    constexpr uint8_t MASK = 0xFF;

    for (int i = 0; i < REG256_WIDTH; i++)
    {
        __m256i correctValue = _mm256_set1_epi8(MASK);

        for (int j = i; j < REG256_WIDTH; j++)
        {
            correctValue.m256i_u8[j] = 0x00;
        }

        __m256i m = GetRightMaskComplementM256U8(i);

        __m256i res = _mm256_cmpeq_epi8(correctValue, m);

        REQUIRE(CheckCompareResult(res));
    }
}

