

#include "catch.hpp"
#include "cpuinfo.h"

#include "liboppcore.h"


using namespace libopp;



TEST_CASE("cpu_info", "[utils]")
{
    const CpuInfo& cpuInfo = CpuInfo::cpuInfo();


}



TEST_CASE("Rect_equal", "[misc]")
{
    Rect r1(1, 2, 3, 4);
    Rect r2(1, 2, 3, 4);

    REQUIRE(r1 == r2);
}
