@echo off

msbuild.exe /p:Platform=x64 /p:Configuration=Release "libopp.sln"
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

mkdir dist
mkdir dist\libopp_test\
copy x64\Release\libopp_test.exe dist\libopp_test\libopp_test.exe
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL% 

